# Team 3 Semester Project

For full documentation visit [our Gitlab](https://gitlab.com/23f-its-projekt/team-3).

## The Project

This project is a project with many layers, and you can read a little about them in the following sections. We will detail the security of **{Company Name Here}** and the failures we have discovered, and how best to fix them.

### Risk Analysis

This project is a risky biscuit. The following are some surface level reasons which will be detailed in a following section.

- 1.
- 2.
- 3.

### Interest Analysis

This project is extremely interesting, and there are many reasons to be interested in it. Those include the following.

- 1.
- 2.
- 3.

## Project layout

    Brainstorming
	Risk Analysis
	Interest Analysis
	