# Baggrund

Baggrunden for projektet er at et nyt storcenter skal opføres, og der er behov for at etablere en pålidelig IT-infrastruktur,
som kan understøtte centerets drift og sikre en god kundeoplevelse. Der skal bl.a. etableres et udlejningssystem 
butikslokaler, og der skal være overvågning af det offentlige Wi-Fi for at sikre, at kunderne kan surfe sikkert. Der er
også behov for at etablere et pålideligt betalingssystem, og det er vigtigt at tage højde for IT-sikkerhed og beskyttelse 
mod cybertrusle

# Formål

Dette projekt har til formål at dykke ned i og udvide vores eksisterende viden
om IT-sikkerhed, med særlig fokus på at optimere den til gavn for de studerende på holdet i foråret 2023.

Vi vil udforske kompleksiteterne i IT-sikkerhed og identificere de nyeste tendenser og bedste praksisser inden for feltet,
med henblik på at opbygge en solid grundviden og give de studerende de nødvendige redskaber til at identificere og tackle 
sikkerhedsrisici på en effektiv måde. Dette projekt vil have en praktisk tilgang, der inkluderer case-studier og hands-on øvelser
for at styrke de studerendes forståelse af vigtigheden af IT-sikkerhed.

# Mål

Målet med projektet er at kunne anvende, udføre, udvælge og implementere praktiske tiltag til sikringen af virksomhedens udstyr samt give efterfølgende supportering af udstyret. 

## Nice to have

1. ..
2. ..
3. ..

## Need to have

1.  grundlæggende netværksdiagram til at oprette forbindelse mellem forskellige enheder og lokaler i storcenteret
2. 	Internetforbindelse og sikkerhedsforanstaltninger til at beskytte netværket og enhederne mod cyberangreb
3.  Backup- og gendannelsesløsninger til at beskytte data og minimere tab i tilfælde af systemsvigt.

# Tidsplan

## uge 8
- projektide
- projektplan

## uge 10
- Research færdiggjort

## uge 13
- Skitsering af projektet (se hvilken retning vi vil gå i)

## uge 17 
- Projektet er færdig opbygget

## uge 20
- Implementering af projektet færdiggjort

## uge 21
- Rapporten skal afleveres 5 juni

## uge 23
- eksamen 19+20 juni

# Organisation

Hele teamet består af studerende indenfor IT-sikkerhed, og de er ansvarlige for at implementere sikkerhedsforanstaltninger og cyberbeskyttelse i infrastrukturen.
Alle opgaver i forbindelse med dette varetages af teamet.  


# Risikostyring

Potentielle risici inkluderer:

- Sygdom kan skade pruduktiviteten af teames arbejde.  
- Uforudsete tekniske udfordringer under implementeringen
- Manglende tid til test og validering af IT-infrastrukturen

For at håndtere disse risici vil teamet:

- Planlægge tid til fejlfinding og løsning af tekniske udfordringer
- Implementere sikkerhedsforanstaltninger og backup- og gendannelsesprocedurer for at reducere risikoen for datatab.
- Prioritere test og validering af IT-infrastrukturen.


# Interessenter

Her skrives resultatet af interessentanalyse

Inspiration kan hentes her [https://www.cfl.dk/artikler/interessentanalyse](https://www.cfl.dk/artikler/interessentanalyse)

# kommunikation

Dette er måden teamet får kontakt med hinanden. 

## Kommunikationskanaler

- gitlab issue board
- element
- discord
- Messenger

## Kommunikationsaktiviteter

- Møder
- Teame beskeder

# Perspektiv

Projektet danner grundlag for uddannelsens øvrige semesterprojekter og skal opfattes som en øvelse i at lave et godt bachelor projekt som afslutning på uddannelsen.

# Evaluering

Hvordan evalueres projektet udover de obligatoriske eksamens afleveringer.  
Det anbefales at evaluere alle projekter ved afslutning for at synliggøre og samle erfaringer til brug i næste projekt.  
Det er også en god ide at, som team, at kigge tilbage på projektets forløb for at reflektere over hvad de enekelte teammedlemmer har lært i løbet af projektet.

Endelig bør et godt udført og veldokumenteret projekt inkluderes i en studerendes portfolio/dokumentation side så det er muligt for andre at få glæde af projeket.

# Referencer

Relevante overordnede referencer til essentielle ting der benyttes i projektet, hver reference bør have en beskrivelse der kort begrunder og forklarer hvordan referencer er relevant for projektet.

- Links til dokumentation
- Links til metoder
- mm.
