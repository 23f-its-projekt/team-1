# Teammøder

## Mødedagsordenen uge 9

Vi afholder vores første teammøde i morgen, den 16/11 kl. 12:30-13:00. Jeg håber, I begge kan deltage. Mødet forventes at vare mellem 15-20 minutter.

1. Statusopdatering
- Hvad arbejder medlemmerne på nu?
- Er der noget, der forhindrer medlemmerne i at fortsætte med deres nuværende arbejde?
- Hvad kan der gøres for at få medlemmerne tilbage på sporet?

2. Indsættelse af aktuelle issues på GitLab.

3. Fordeling af issues mellem medlemmerne.

4. Hvis nogen medlemmer har noget at tilføje, vil det blive taget op i dette punkt.

___
___
*__notation af møde__*

__Teammøde 1 – 1/3 – 2023__

Statusopdatering:
- Thobias – Dialog med personen som har opbygget netværket hos rosengårdcenteret. Har fået nogen små detaljer omkring dele af netværket. Arbejde videre med research, og efterspørgsel omkring information omkring Rosengårdcenteret.
- Morten – Kigget på forskellige subnets, eks. Overvågning og økonomiafdeling. Arbejde videre med research.
- Sune - Kigget på forskellige subnets, eks. Overvågning og økonomiafdeling. Arbejde videre med research.

__Ting vi skal arbejde med__ 

Service og systemer (beskyttelse af dem)

Issues: 
Opsætning af issues, som skal smækkes ind i boardet på gitlab. 

TODO:

Skriv forskellige issues ind, så vi har noget at arbejde med. 


___ 

___

